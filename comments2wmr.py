#!/usr/bin/env python3
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)
import sqlite3
import xml.etree.ElementTree as ET

XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dsq="http://www.disqus.com/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:wp="http://wordpress.org/export/1.0/"
>
  <channel>
  </channel>
</rss>
"""

"""
Complete database scheme:

CREATE TABLE "auth_permission" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(50) NOT NULL,
    "content_type_id" integer NOT NULL,
    "codename" varchar(100) NOT NULL,
    UNIQUE ("content_type_id", "codename")
);
CREATE TABLE "auth_group_permissions" (
    "id" integer NOT NULL PRIMARY KEY,
    "group_id" integer NOT NULL,
    "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"),
    UNIQUE ("group_id", "permission_id")
);
CREATE TABLE "auth_group" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(80) NOT NULL UNIQUE
);
CREATE TABLE "auth_user_user_permissions" (
    "id" integer NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"),
    UNIQUE ("user_id", "permission_id")
);
CREATE TABLE "auth_user_groups" (
    "id" integer NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "group_id" integer NOT NULL REFERENCES "auth_group" ("id"),
    UNIQUE ("user_id", "group_id")
);
CREATE TABLE "auth_user" (
    "id" integer NOT NULL PRIMARY KEY,
    "username" varchar(30) NOT NULL UNIQUE,
    "first_name" varchar(30) NOT NULL,
    "last_name" varchar(30) NOT NULL,
    "email" varchar(75) NOT NULL,
    "password" varchar(128) NOT NULL,
    "is_staff" bool NOT NULL,
    "is_active" bool NOT NULL,
    "is_superuser" bool NOT NULL,
    "last_login" datetime NOT NULL,
    "date_joined" datetime NOT NULL
);
CREATE TABLE "django_admin_log" (
    "id" integer NOT NULL PRIMARY KEY,
    "action_time" datetime NOT NULL,
    "user_id" integer NOT NULL REFERENCES "auth_user" ("id"),
    "content_type_id" integer,
    "object_id" text,
    "object_repr" varchar(200) NOT NULL,
    "action_flag" smallint unsigned NOT NULL,
    "change_message" text NOT NULL
);
CREATE TABLE "django_site" (
    "id" integer NOT NULL PRIMARY KEY,
    "domain" varchar(100) NOT NULL,
    "name" varchar(50) NOT NULL
);
CREATE TABLE "django_comments" (
    "id" integer NOT NULL PRIMARY KEY,
    "content_type_id" integer NOT NULL,
    "object_pk" text NOT NULL,
    "site_id" integer NOT NULL REFERENCES "django_site" ("id"),
    "user_id" integer REFERENCES "auth_user" ("id"),
    "user_name" varchar(50) NOT NULL,
    "user_email" varchar(75) NOT NULL,
    "user_url" varchar(200) NOT NULL,
    "comment" text NOT NULL,
    "submit_date" datetime NOT NULL,
    "ip_address" char(15),
    "is_public" bool NOT NULL,
    "is_removed" bool NOT NULL
);
CREATE TABLE "django_comment_flags" (
    "id" integer NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL REFERENCES "auth_user" ("id"),
    "comment_id" integer NOT NULL REFERENCES "django_comments" ("id"),
    "flag" varchar(30) NOT NULL,
    "flag_date" datetime NOT NULL,
    UNIQUE ("user_id", "comment_id", "flag")
);
CREATE TABLE "django_session" (
    "session_key" varchar(40) NOT NULL PRIMARY KEY,
    "session_data" text NOT NULL,
    "expire_date" datetime NOT NULL
);
CREATE TABLE "django_content_type" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "app_label" varchar(100) NOT NULL,
    "model" varchar(100) NOT NULL,
    UNIQUE ("app_label", "model")
);
CREATE TABLE "tagging_tag" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(50) NOT NULL UNIQUE
);
CREATE TABLE "tagging_taggeditem" (
    "id" integer NOT NULL PRIMARY KEY,
    "tag_id" integer NOT NULL REFERENCES "tagging_tag" ("id"),
    "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"),
    "object_id" integer unsigned NOT NULL,
    UNIQUE ("tag_id", "content_type_id", "object_id")
);
CREATE TABLE "zinnia_category" (
    "id" integer NOT NULL PRIMARY KEY,
    "title" varchar(255) NOT NULL,
    "slug" varchar(255) NOT NULL UNIQUE,
    "description" text NOT NULL,
    "parent_id" integer,
    "lft" integer unsigned NOT NULL,
    "rght" integer unsigned NOT NULL,
    "tree_id" integer unsigned NOT NULL,
    "level" integer unsigned NOT NULL
);
CREATE TABLE "zinnia_entry_sites" (
    "id" integer NOT NULL PRIMARY KEY,
    "entry_id" integer NOT NULL,
    "site_id" integer NOT NULL REFERENCES "django_site" ("id"),
    UNIQUE ("entry_id", "site_id")
);
CREATE TABLE "zinnia_entry_related" (
    "id" integer NOT NULL PRIMARY KEY,
    "from_entry_id" integer NOT NULL,
    "to_entry_id" integer NOT NULL,
    UNIQUE ("from_entry_id", "to_entry_id")
);
CREATE TABLE "zinnia_entry_authors" (
    "id" integer NOT NULL PRIMARY KEY,
    "entry_id" integer NOT NULL,
    "author_id" integer NOT NULL REFERENCES "auth_user" ("id"),
    UNIQUE ("entry_id", "author_id")
);
CREATE TABLE "zinnia_entry_categories" (
    "id" integer NOT NULL PRIMARY KEY,
    "entry_id" integer NOT NULL,
    "category_id" integer NOT NULL REFERENCES "zinnia_category" ("id"),
    UNIQUE ("entry_id", "category_id")
);
CREATE TABLE "zinnia_entry" (
    "id" integer NOT NULL PRIMARY KEY,
    "title" varchar(255) NOT NULL,
    "content" text NOT NULL,
    "slug" varchar(255) NOT NULL,
    "status" integer NOT NULL,
    "start_publication" datetime,
    "end_publication" datetime,
    "creation_date" datetime NOT NULL,
    "last_update" datetime NOT NULL,
    "comment_enabled" bool NOT NULL,
    "pingback_enabled" bool NOT NULL,
    "trackback_enabled" bool NOT NULL,
    "comment_count" integer NOT NULL,
    "pingback_count" integer NOT NULL,
    "trackback_count" integer NOT NULL,
    "excerpt" text NOT NULL,
    "image" varchar(100) NOT NULL,
    "featured" bool NOT NULL,
    "tags" varchar(255) NOT NULL,
    "login_required" bool NOT NULL,
    "password" varchar(50) NOT NULL,
    "content_template" varchar(250) NOT NULL,
    "detail_template" varchar(250) NOT NULL
);
"""


class Record:
    def __init__(self):
        pass

    @classmethod
    def get(cls, objid):
        """ factory to find object by id

        @param site_id: integer id of the user
        @return return: new object of the Site class
        """
        db.c.execute('SELECT * FROM %s WHERE id=?' % cls.table_name,
                     (objid,))
        return cls(db.c.fetchone())


class Site(Record):
    """ Class representing django site

    CREATE TABLE "django_site" (
        "id" integer NOT NULL PRIMARY KEY,
        "domain" varchar(100) NOT NULL,
        "name" varchar(50) NOT NULL
    );

    """
    table_name = 'django_site'

    def __init__(self, in_row):
        """ Class initialiser """
        super().__init__()
        if in_row is not None:
            self.site_id = in_row['id']
            self.domain = in_row['domain']
            self.name = in_row['name']


class Article(Record):
    """ Class representing one blog post

    CREATE TABLE "zinnia_entry" (
        "id" integer NOT NULL PRIMARY KEY,
        "title" varchar(255) NOT NULL,
        "content" text NOT NULL,
        "slug" varchar(255) NOT NULL,
        "status" integer NOT NULL,
        "start_publication" datetime,
        "end_publication" datetime,
        "creation_date" datetime NOT NULL,
        "last_update" datetime NOT NULL,
        "comment_enabled" bool NOT NULL,
        "pingback_enabled" bool NOT NULL,
        "trackback_enabled" bool NOT NULL,
        "comment_count" integer NOT NULL,
        "pingback_count" integer NOT NULL,
        "trackback_count" integer NOT NULL,
        "excerpt" text NOT NULL,
        "image" varchar(100) NOT NULL,
        "featured" bool NOT NULL,
        "tags" varchar(255) NOT NULL,
        "login_required" bool NOT NULL,
        "password" varchar(50) NOT NULL,
        "content_template" varchar(250) NOT NULL,
        "detail_template" varchar(250) NOT NULL
    );

    """
    table_name = 'zinnia_entry'

    def __init__(self, in_row):
        """
        @param param: param_desc
        @return return: return_desc
        """
        super().__init__()
        if in_row is not None:
            self.article_id = in_row['id']
            self.title = in_row['title']
            self.content = in_row['content']
            self.slug = in_row['slug']

    def generate_item(self, item):
        """
        <title>Foo bar</title>
      <!-- absolute URI to article -->
      <link>http://foo.com/example</link>
      <!-- body of the page or post; use cdata; html allowed (though will be formatted to DISQUS specs) -->
      <content:encoded><![CDATA[Hello world]]></content:encoded>
      <!-- value used within disqus_identifier; usually internal identifier of article -->
      <dsq:thread_identifier>disqus_identifier</dsq:thread_identifier>
      <!-- creation date of thread (article), in GMT. Must be YYYY-MM-DD HH:MM:SS 24-hour format. -->
      <wp:post_date_gmt>2010-09-20 09:13:44</wp:post_date_gmt>
      <!-- open/closed values are acceptable -->
      <wp:comment_status>open</wp:comment_status>
        """
        title = ET.SubElement(item, 'title')
        title.text = self.title


class User(Record):
    """ Class representing one user

        CREATE TABLE "auth_user" (
            "id" integer NOT NULL PRIMARY KEY,
            "username" varchar(30) NOT NULL UNIQUE,
            "first_name" varchar(30) NOT NULL,
            "last_name" varchar(30) NOT NULL,
            "email" varchar(75) NOT NULL,
            "password" varchar(128) NOT NULL,
            "is_staff" bool NOT NULL,
            "is_active" bool NOT NULL,
            "is_superuser" bool NOT NULL,
            "last_login" datetime NOT NULL,
            "date_joined" datetime NOT NULL
        );
    """
    table_name = 'auth_user'

    def __init__(self, in_row):
        """ Class initialiser """
        super().__init__()
        if in_row is not None:
            self.user_id = in_row['id']
            self.username = in_row['username']
            self.first_name = in_row['first_name']
            self.last_name = in_row['last_name']
            self.email = in_row['email']
            self.password = in_row['password']
            self.is_staff = in_row['is_staff']
            self.is_active = in_row['is_active']
            self.is_superuser = in_row['is_superuser']
            self.last_login = in_row['last_login']
            self.date_joined = in_row['date_joined']

    @property
    def user_name(self):
        return self.username

    @property
    def user_email(self):
        return self.email

    @property
    def user_url(self):
        raise NotImplementedError


class Flags(Record):
    """ Class representing comment flags

        CREATE TABLE "django_comment_flags" (
            "id" integer NOT NULL PRIMARY KEY,
            "user_id" integer NOT NULL REFERENCES "auth_user" ("id"),
            "comment_id" integer NOT NULL REFERENCES "django_comments" ("id"),
            "flag" varchar(30) NOT NULL,
            "flag_date" datetime NOT NULL,
            UNIQUE ("user_id", "comment_id", "flag")
        );
    """

    def __init__(self):
        """ Class initialiser """
        super().__init__()


class Comment(Record):
    """ Class representing one comment """

    def __init__(self, in_row):
        """ Class initialiser

        @param in_row sqlite3.Row with keys
            ['id', 'content_type_id', 'object_pk', 'site_id',
             'user_id', 'user_name', 'user_email', 'user_url',
             'comment', 'submit_date', 'ip_address',
             'is_public', 'is_removed']

        CREATE TABLE "django_comments" (
            "id" integer NOT NULL PRIMARY KEY,
            "content_type_id" integer NOT NULL,
            "object_pk" text NOT NULL,
            "site_id" integer NOT NULL REFERENCES "django_site" ("id"),
            "user_id" integer REFERENCES "auth_user" ("id"),
            "user_name" varchar(50) NOT NULL,
            "user_email" varchar(75) NOT NULL,
            "user_url" varchar(200) NOT NULL,
            "comment" text NOT NULL,
            "submit_date" datetime NOT NULL,
            "ip_address" char(15),
            "is_public" bool NOT NULL,
            "is_removed" bool NOT NULL
        );

        (1, 13, '1', 1, None, 'Philipp Kewisch', '', '', 'You
        should have noted that it adds a signature to bugzilla
        comments, or at least could have made it do that when
        loading the page instead of just before commenting. This
        made me add the fedora bugzappers signature to my
        bugzilla...', '2010-03-03 11:07:03', '', 1, 0)

        """
        super().__init__()
        self.comment_id = in_row['id']
        self.site = Site.get(in_row['site_id'])
        self.user = User.get(in_row['user_id'])
        self.user.username = in_row['user_name']
        self.user.email = in_row['user_email']
        self.comment = in_row['comment']
        self.submit_date = in_row['submit_date']
        self.ip_address = in_row['ip_address']
        self.is_public = in_row['is_public']
        self.is_removed = in_row['is_removed']

    def process(self, xout):
        """ process one comment
        """
        pass

class DataBase():
    """ Processing whole database """

    def __init__(self, filename='sqlite3.db'):
        """ Class initialiser """
        self.conn = sqlite3.connect(filename)
        self.conn.row_factory = sqlite3.Row
        self.c = self.conn.cursor()
        self.xml_out = ET.fromstring(XML_TEMPLATE)

    def process_comments(self):
        """ Go through all comments

        """
        # FIXME This is wrong, we have to start with articles, not comments
        self.c.execute('SELECT * FROM django_comments')
        for cm_row in self.c.fetchall():
            cm = Comment(cm_row)
            item = ET.SubElement(self.xml_out, 'item')
            cm.process(item)

db = DataBase()

if __name__ == '__main__':
    db.process_comments()
